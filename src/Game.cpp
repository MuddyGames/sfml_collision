#include <./include/Game.h>
#include <SFML/Graphics.hpp>

using namespace sf;

sf::RenderWindow* window = new RenderWindow(sf::VideoMode(800, 600), "Launching...");;

sf::CircleShape player_shape;
sf::CircleShape npc_shape;

Game::Game(){}

void Game::initialize()
{
	window->setSize(sf::Vector2u(640, 480));
	window->setTitle("Game");

	player = new Player();
	player_shape.setRadius(50.0f);
	player_shape.setFillColor(sf::Color(0, 255, 0));
	player_shape.setOutlineThickness(10);
	player_shape.setOutlineColor(sf::Color(0, 0, 255));

	npc = new NPC();
	npc_shape.setRadius(100.0f);
	npc_shape.setFillColor(sf::Color(0, 0, 255));
	npc_shape.setOutlineThickness(10);
	npc_shape.setOutlineColor(sf::Color(0, 255, 0));

	npc_shape.setPosition(sf::Vector2f(50.0f, 50.0f));

	npc->setX(npc_shape.getPosition().x);
	npc->setY(npc_shape.getPosition().y);
	npc->setR(npc_shape.getRadius());
}

void Game::run()
{
	initialize();

	while (window->isOpen())
	{
		update();
		draw();
	}
}

void Game::update()
{
	sf::Event event;
	while (window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window->close();

		// Update Based on Mouse Position
		player_shape.setPosition(window->mapPixelToCoords(sf::Mouse::getPosition(*window)));

		player->setX(player_shape.getPosition().x);
		player->setY(player_shape.getPosition().y);
		player->setR(player_shape.getRadius());

		if(player->collision(npc)){
			npc_shape.setOutlineColor(sf::Color(255, 0, 0));
		}
		else{
			npc_shape.setOutlineColor(sf::Color(0, 255, 0));
		}

		player->update();
		npc->update();

	}

}

void Game::draw()
{
	window->clear();
	window->draw(player_shape);
	window->draw(npc_shape);
	window->display();
}


